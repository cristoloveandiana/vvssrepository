package agenda.test;

import static org.junit.Assert.*;

import com.sun.org.apache.xpath.internal.SourceTree;
//import org.apache.commons.lang.StringUtils;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;

	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}

	@Test
	public void testCase1() {
		try {
			con = new Contact("name", "address1", "+4071122334455", "test1@email.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for (Contact c : rep.getContacts())
			if (c.equals(con)) {
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}

	@Test
	public void testCase2() {
		try {
			rep.addContact((Contact) new Object());
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testCase3() {
		for (Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("name", "address1", "+071122334455", "test1@email.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.counContacts();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
		else assertTrue(false);
	}


	@Test
	public void ecpValidTestCase1() {
		try {
			con = new Contact("name", "address1", "+4071122334455", "test1@email.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for (Contact c : rep.getContacts())
			if (c.equals(con)) {
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}

	@Test
	public void ecpValidTestCase2() {
		try {
			con = new Contact("name", "address2", "+4071122334455", "test1@email.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for (Contact c : rep.getContacts())
			if (c.equals(con)) {
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}

	@Test
	public void ecpInvalidTestCase1() {
		try {
			Contact con2 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
//            Contact con3 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
//            Contact con4 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
//            Contact con5 = new Contact("name", "address1", "+4071122334455", "test1@email.com");

			rep.addContact(con2);
//            rep.addContact(con3);
//            rep.addContact(con4);
//            rep.addContact(con5);
			if (rep.counContacts() == rep.counContacts() + 1) {
				assertTrue(false);
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
	}

	@Test
	public void ecpInvalidTestCase2() {
		try {
			Contact con2 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
			Contact con3 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
//
			rep.addContact(con3);
			rep.addContact(con2);

			if (rep.counContacts() == rep.counContacts() + 2) {
				assertTrue(false);
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
	}


	@Test
	public void ecpInvalidTestCase3() {
		try {
			Contact con2 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
			Contact con3 = new Contact("name", "address1", "+4071122334455", "test1@email.com");
			Contact con4 = new Contact("name", "address1", "+4071122334455", "test1@email.com");

			rep.addContact(con2);
			rep.addContact(con3);
			rep.addContact(con4);
			if (rep.counContacts() == rep.counContacts() + 3) {
				assertTrue(false);
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
	}

/*
	@Test
	public void bvaInvalidTestCase1() {
		try {
			Contact con2 = new Contact(StringUtils.repeat("M", 256), "address1", "+4071122334455", "test1@email.com");
			Contact con3 = new Contact("name", StringUtils.repeat("D",256), "+4071122334455", "test1@email.com");


		} catch (InvalidFormatException e) {
			assertTrue(true);
//            System.out.println(e.getMessage());
		}
	}
*/
/*
	@Test
	public void bvaInvalidTestCase2() {
		try {
			Contact con2 = new Contact(StringUtils.repeat("M", 256), "address1", "+4071122334455", "test1@email.com");


		} catch (InvalidFormatException e) {
			assertTrue(true);
//            System.out.println(e.getMessage());
		}
	}
*/
	@Test
	public void bvaValidTestCase() {
		try {
			Contact con2 = new Contact("M", "address1", "+4071122334455", "test1@email.com");
			Contact con3 = new Contact("M...123", "address1", "+4071122334455", "test1@email.com");
			Contact con4 = new Contact("name", "D", "+4071122334455", "test1@email.com");
			Contact con5 = new Contact("name", "D...1234", "+4071122334455", "test1@email.com");

			rep.addContact(con2);
			rep.addContact(con3);
			rep.addContact(con5);
			rep.addContact(con4);

			if (rep.counContacts() == rep.counContacts() + 4) {
				assertTrue(false);
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
	}

	@Test
	public void bvaValidTestCase2() {
		try {
			Contact con3 = new Contact("M...123", "address1", "+4071122334455", "test1@email.com");
			Contact con4 = new Contact("name", "D", "+4071122334455", "test1@email.com");
			Contact con5 = new Contact("name", "D...1234", "+4071122334455", "test1@email.com");

			rep.addContact(con3);
			rep.addContact(con5);
			rep.addContact(con4);

			if (rep.counContacts() == rep.counContacts() + 4) {
				assertTrue(false);
				System.out.println("Inserare reusita");
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void bvaValidTestCase3() {
		try {
			Contact con2 = new Contact("M", "address1", "+4071122334455", "test1@email.com");
			Contact con3 = new Contact("M...123", "address1", "+4071122334455", "test1@email.com");
			Contact con4 = new Contact("name", "D", "+4071122334455", "test1@email.com");

			rep.addContact(con2);
			rep.addContact(con3);
			rep.addContact(con4);

			if (rep.counContacts() == rep.counContacts() + 4) {
				assertTrue(false);
			}

		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
	}


}
